# Python 반복기와 생성기: 용법과 예 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 지연 연산(lazy evaluation)를 위한 반복기와 생성기 생성과 사용 방법에 대해 알아본다.</font>

## 목차

1. [개요](./iterators-and-generators.md#intro)
1. [반복기와 생성기란?](./iterators-and-generators.md#sec_02)
1. [반복기 생성과 사용 방법](./iterators-and-generators.md#sec_03)
1. [생성기 생성과 사용 방법](./iterators-and-generators.md#sec_04)
1. [반복기와 생성기의 차이점과 유사점](./iterators-and-generators.md#sec_05)
1. [반복기와 생성기의 장단점](./iterators-and-generators.md#sec_06)
1. [반복기와 생성기의 실제 사례](./iterators-and-generators.md#sec_07)
1. [요약](./iterators-and-generators.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 27 — Python Iterators and Generators: Usage and Examples](https://python.plainenglish.io/python-tutorial-27-python-iterators-and-generators-usage-and-examples-ca9b9dc1fa4b?sk=c8fdb4c691ea269a29732206f230df73)를 편역한 것이다.
