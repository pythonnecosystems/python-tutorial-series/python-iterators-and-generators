# Python 반복기와 생성기: 용법과 예

## <a name="intro"></a> 개요
Python 반복기(iterator)와 생성기(generator)에 대한 이 포스팅에서는 다음과 같은 내용을 배울 수 있다.

- 반복자와 생성자는 무엇이며 Python에서 작동 방식
- 지연 평가를 위해 반복기와 생성기를 생성하고 사용하는 방법
- 반복기와 발전기의 차이점과 유사점
- 반복기와 발전기의 장점과 단점
- 반복기와 생성기를 실제 사례에 적용 방법

이 포스팅의 내용을 이해하면 Python의 이러한 강력한 기능과 코드에서 효과적으로 사용하는 방법에 대해 더 잘 이해할 수 있을 것이다.

그러나 세부 사항으로 뛰어들기 전에, 먼저 간단한 질문으로 시작해보자. 반복기란 무엇인가?

## <a name="sec_02"></a> 반복기와 생성기란?
반복기는 반복할 수 있는 객체로, 반복할 수 있는 모든 값을 탐색할 수 있다. Python에서 반복기는 **iterator protocol**을 구현하는 객체로, 두 가지 메서드로 이루어진다.

- **`__iter__()`**: 이 메서드는 반복기 객체 자체를 반환한다. `for`와 `in` 문에서 사용된다.
- **`__next__()`**: 이 메서드는 반복기에서 다음 값을 반환한다. 더 이상 값이 없으면 `StopIteration` 예외를 발생시킨다. 이 메서드는 `next()` 함수에서 사용된다.

생성기는 **생성기 함수(generator function)** 또는 **제너레이터 표현식(generator expression)**을 사용하여 생성되는 특수한 유형의 반복기이다. 제너레이터 함수는 **`yield`** 키워드를 사용하여 값을 반환하고 함수의 실행을 중단하는 함수이다. 제너레이터 표현식은 리스트 컴프리헨션(list comprehension)과 유사하게 제너레이터 객체를 생성하기 위한 간결한 구문이다.

생성기와 정규 반복기(regular)의 가장 큰 차이점은 생성기가 모든 값을 메모리에 저장하지 않고 즉시 생성한다는 것이다. 이는 생성기가 메모리를 다 소모하지 않고 무한한 일련의 값을 생성할 수 있다는 것을 의미한다. 또한 값이 캐시되거나 저장되지 않기 때문에 생성기가 한 번만 반복될 수 있다.

반복기와 생성기의 개념을 설명하기 위해 예를 살펴보자.

## <a name="sec_03"></a> 반복기 생성과 사용 방법
Python에서 반복기를 만들려면 클래스에서 반복기 프로토콜(iterator protocol)을 구현해야 한다. 이는 클래스에서 `__iter__()`와 `__next__()` 메서드를 정의해야 함을 의미한다. `__iter__()` 메서드는 반복기 객체 자체를 반환하고 `__next__()` 메서드는 반복기에서 다음 값을 반환해야 한다. 값이 더 이상 없으면 `__next__()` 메서드는 `StopIteration` 예외를 발생시켜야 한다.

Fibonacci 수열을 반환하는 반복기를 만드는 방법의 예를 보자. Fibonacci 수열은 각각의 숫자가 앞의 두 숫자의 합인 0과 1에서 시작하는 일련의 숫자이다. 예를 들어, Fibonacci 수열의 처음 10개의 숫자는 다음과 같다. 0, 1, 2, 3, 5, 8, 13, 21, 34.

```python
# Define a class that implements the iterator protocol
class Fibonacci:
    # Initialize the class with two attributes: a and b
    def __init__(self):
        self.a = 0
        self.b = 1
    # Define the __iter__() method that returns the iterator object itself
    def __iter__(self):
        return self
    # Define the __next__() method that returns the next value from the iterator
    def __next__(self):
        # Store the current value of a in a variable
        value = self.a
        # Update the values of a and b using the Fibonacci formula
        self.a, self.b = self.b, self.a + self.b
        # Return the current value
        return value
```

이제 `Fibonacci` 클래스의 인스턴스를 만들어 반복기로 사용할 수 있다. 우리는 `__next__()` 함수를 사용하여 반복기에서 다음 값을 얻을 수도 있고, `for` 루프를 사용하여 반복기를 이용하여 반복할 수도 있다. 그 예는 다음과 같다.

```python
# Create an instance of the Fibonacci class
fib = Fibonacci()
# Use the next() function to get the next value from the iterator
print(next(fib)) # 0
print(next(fib)) # 1
print(next(fib)) # 1
print(next(fib)) # 2
print(next(fib)) # 3
# Use a for loop to iterate over the iterator
for i in fib:
    # Break the loop if the value is greater than 100
    if i > 100:
        break
    # Print the value
    print(i)
```

위의 코드 출력은 아래와 같다.

```
0
1
1
2
3
5
8
13
21
34
55
89
```

보다시피 모든 값을 메모리에 저장하지 않고 Fibonacci 수열을 즉시 생성하는 반복기를 만들었다. 이것은 지연 평가의 한 예이며, 이는 값을 미리 계산하는 것이 아니라 필요할 때만 계산한다는 것을 의미한다. 이것은 특히 크거나 무한한 수열을 다룰 때 메모리를 절약하고 성능을 향상시킬 수 있다.

## <a name="sec_04"></a> 생성기 생성과 사용 방법
Python에서 생성기를 만들려면 **생성기 함수(generator function)** 또는 **생성자 표현식(generator expression)**을 사용할 수 있다. 생성기 함수는 **`yield`** 키워드를 사용하여 값을 반환하고 함수의 실행을 중단하는 함수이다. 생성기 표현식은 리스트 컴프리헨션과 유사하게 생성기 객체를 생성하기 위한 간결한 구문이다.

Fibonacci 수열을 반환하는 생성기 함수를 만드는 방법의 예를 보자. 생성기 함수는 이전 절에서 정의한 반복기 클래스와 매우 유사하지만, **`return`** 키워드 대신에 **`yield`** 키워드를 사용한다. 예를 들면 다음과 같다.

```python
# Define a generator function that returns the Fibonacci sequence
def fibonacci():
    # Initialize two variables: a and b
    a = 0
    b = 1
    # Use an infinite loop to generate the values
    while True:
        # Yield the current value of a
        yield a
        # Update the values of a and b using the Fibonacci formula
        a, b = b, a + b
```

이제, 생성기 함수를 호출함으로써 생성기 객체를 만들고 그것을 반복기로 사용할 수 있다. 우리는 이전과 같은 방법을 사용하여 생성기로부터 다음 값을 얻거나 생성기를 사용하여 반복할 수 있다. 예를 들면, 다음과 같다.

```python
# Create a generator object by calling the generator function
fib = fibonacci()
# Use the next() function to get the next value from the generator
print(next(fib)) # 0
print(next(fib)) # 1
print(next(fib)) # 1
print(next(fib)) # 2
print(next(fib)) # 3
# Use a for loop to iterate over the generator
for i in fib:
    # Break the loop if the value is greater than 100
    if i > 100:
        break
    # Print the value
    print(i)
```

위의 코드 출력은 이전과 같다.

```
0
1
1
2
3
5
8
13
21
34
55
89
```

보다시피, 모든 값을 메모리에 저장하지 않고 Fibonacci 수열을 즉시 생성하는 생성기를 만들었다. 이것은 지연 평가의 또 다른 예이다. 이는 값이 미리 계산되는 것이 아니라 필요할 때만 계산된다는 것을 의미한다.

생성기 표현식은 생성기 함수를 정의하지 않고 생성기 객체를 생성하는 보다 간단한 방법이다. 생성기 표현식은 리스트 컴프리헨션과 유사하지만 대괄호(`[]`) 대신 소괄호(`()`)를 사용하여 메모리에 리스트를 생성하는 것이 아니라 한 번에 값 하나를 생성하는 반복기이다. 예를 들어, 처음 10개의 자연수의 제곱을 반환하는 생성기 표현식을 다을과 같이 만들 수 있다.

```python
# Create a generator expression that returns the squares of the first 10 natural numbers
squares = (x**2 for x in range(1, 11))
# Use the next() function to get the next value from the generator expression
print(next(squares)) # 1
print(next(squares)) # 4
print(next(squares)) # 9
print(next(squares)) # 16
print(next(squares)) # 25
# Use a for loop to iterate over the generator expression
for i in squares:
    # Print the value
    print(i)
```

위의 코드 출력은 아래와 같다.

```
1
4
9
16
25
36
49
64
81
100
```

보다시피, 처음 10개 자연수의 제곱을 메모리의 리스트에 저장하지 않고 바로 생성하는 생성기 식을 만들었다. 이것은 지연 평가의 또 다른 예이다. 이는 값을 미리 계산하는 것이 아니라 필요할 때만 계산한다는 것을 의미한다.

## <a name="sec_05"></a> 반복기와 생성기의 차이점과 유사점
본 절에서는 Python에서 반복기와 생성기의 차이점과 유사점을 비교, 대조할 것이다. 다음과 같은 측면에서 살펴볼 것이다.

- **Creation(생성)**: 반복기 또는 생성기를 생성하는 방법
- **메모리**: 반복기 또는 생성기가 사용하는 메모리의 크기
- **성능**: 반복기 또는 생성기의 값 생성 속도
- **재사용성**: 반복기 또는 생성기를 몇 번 반복 사용할 할 수 있을까?
- **기능**: 반복기 또는 생성기 사용의 장점과 단점

생성의 측면부터 살펴보도록 하겠다.

```python
# Creation
# Iterator: Iterators are created using iter() and next() functions.
# Generator: Generators are created using functions with yield keyword.

# Memory
# Iterator: Iterators consume less memory because they generate values one at a time.
# Generator: Generators also consume less memory for the same reason.

# Performance
# Iterator: Iterators can be slower compared to generators because they need to implement the next() method.
# Generator: Generators can be faster as they generate values on demand without the need for an explicit next() call.

# Reusability
# Iterator: Iterators can only be iterated over once. Once all values are generated, the iterator is exhausted.
# Generator: Generators can be iterated over multiple times. They maintain their state and can yield values again.

# Functionality
# Iterator: Iterators are useful for iterating over large sequences of data or infinite sequences.
# Generator: Generators are more versatile as they can be used to create iterators, generate infinite sequences, and implement coroutine patterns.

# Let's create an example iterator and generator to illustrate these differences:
class MyIterator:
    def __init__(self, data):
        self.data = data
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= len(self.data):
            raise StopIteration
        value = self.data[self.index]
        self.index += 1
        return value

class MyGenerator:
    def __init__(self, data):
        self.data = data

    def generate(self):
        for value in self.data:
            yield value

# Example usage:
data = [1, 2, 3, 4, 5]

# Iterator
my_iterator = MyIterator(data)
for value in my_iterator:
    print(value)

# Generator
my_generator = MyGenerator(data)
for value in my_generator.generate():
    print(value)
```

## <a name="sec_06"></a> 반복기와 생성기의 장단점
본 절에서는 Python에서 반복기와 생성기를 사용할 경우의 장단점에 대해 논의할 것이다. 다음과 같은 측면에서 살펴볼 것이다.

- **메모리 효율성**: 반복기 또는 생성기가 리스트 또는 튜플과 비교하여 얼마나 많은 메모리를 소비하는지
- **성능**: 반복기 또는 생성기가 리스트 또는 튜플과 비교하여 얼마나 빠르게 값을 생성할 수 있는지
- **재사용성**: 반복기 또는 생성기가 리스트 또는 튜플과 비교하여 몇 번 반복될 수 있는지.
- **기능**: 반복기 또는 생성기를 다양한 용도로 사용할 경우의 장단점은 무엇인가.

메모리 효율 측면부터 살펴보겠다.

```python
# Memory efficiency
# Iterators: Iterators consume less memory compared to lists or tuples because they generate values on demand.
# Generators: Generators also consume less memory for the same reason.

# Performance
# Iterators: Iterators can be slower compared to lists or tuples because they generate values one at a time.
# Generators: Generators can be faster than iterators and lists or tuples because they generate values on demand without the need to store them in memory.

# Reusability
# Iterators: Iterators can only be iterated over once. Once all values are generated, the iterator is exhausted.
# Generators: Generators can be iterated over multiple times. They maintain their state and can yield values again.

# Functionality
# Iterators: Iterators are useful for iterating over large sequences of data or infinite sequences.
# Generators: Generators are more versatile as they can be used to create iterators, generate infinite sequences, and implement coroutine patterns.

# Let's create an example to illustrate the memory efficiency, performance, and reusability of iterators and generators:
import sys

# Memory efficiency
data = [x for x in range(10000)]  # List of 10000 integers
iterator = iter(data)
generator = (x for x in range(10000))

print("Memory usage for list:", sys.getsizeof(data))
print("Memory usage for iterator:", sys.getsizeof(iterator))
print("Memory usage for generator:", sys.getsizeof(generator))

# Performance
import time

# Performance comparison for iteration
start_time = time.time()
for value in data:
    pass
list_time = time.time() - start_time

start_time = time.time()
for value in iterator:
    pass
iterator_time = time.time() - start_time

start_time = time.time()
for value in generator:
    pass
generator_time = time.time() - start_time

print("Time taken for list iteration:", list_time)
print("Time taken for iterator iteration:", iterator_time)
print("Time taken for generator iteration:", generator_time)

# Reusability
# Lists and tuples can be iterated over multiple times
print("List can be iterated over multiple times")
for _ in range(2):
    for value in data:
        pass

# Iterators can only be iterated over once
print("Iterator can only be iterated over once")
for _ in range(2):
    for value in iterator:
        pass

# Generators can be iterated over multiple times
print("Generator can be iterated over multiple times")
for _ in range(2):
    for value in generator:
        pass
```

## <a name="sec_07"></a> 반복기와 생성기의 실제 사례
이 절에서는 Python에서 반복기와 생성기를 서로 다른 용도로 사용하는 실용적인 예를 들 것이다. 다음의 예들을 살펴볼 것이다.

- **대용량 파일 읽기**: 생성기를 사용하여 메모리에 전체 파일을 로드하지 않고 큰 파일을 한 줄 한 줄 읽는 방법
- **순열 생성**: 반복기를 사용하여 주어진 시퀀스의 가능한 모든 순열을 리스트에 저장하지 않고 생성하는 방법
- **실행 평균 계산**: 이전 값을 추적하지 않고 생성기 식을 사용하여 숫자 스트림의 실행 평균을 계산하는 방법

첫 번째 예인 대용량 파일 읽기부터 보겠다.

```python
# Reading large files
def read_large_file(file_path):
    with open(file_path, 'r') as file:
        for line in file:
            yield line.strip()

# Example usage
file_path = 'large_file.txt'
for line in read_large_file(file_path):
    process_line(line)

# Generating permutations
def permutations(seq):
    if len(seq) <= 1:
        yield seq
    else:
        for perm in permutations(seq[1:]):
            for i in range(len(perm) + 1):
                yield perm[:i] + seq[0:1] + perm[i:]

# Example usage
sequence = [1, 2, 3]
for perm in permutations(sequence):
    print(perm)

# Calculating running averages
def running_average(numbers):
    total = 0
    count = 0
    for num in numbers:
        total += num
        count += 1
        yield total / count

# Example usage
numbers = [1, 2, 3, 4, 5]
for avg in running_average(numbers):
    print(avg)
```

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 반복자기와 생성기를 만들고 사용하는 방법을 설명하였다. 이 두 개념 사이의 차이점과 유사점, 그리고 그것들이 지연 평가, 메모리 효율성, 성능 향상에 어떻게 도움이 될 수 있는지 살펴보았다. 또한 대용량 파일 읽기, 순열 생성, 실행 평균 계산과 같이 반복기와 생성기를 다양한 문제에 적용하는 몇 가지 실용적인 예도 보았다.

Python의 이러한 강력한 기능에 대해 이해하여 작용할 수 있기를 바란다. 만약 반복기와 생성기에 대해 더 알고 싶다면, 다음의 자료를 참고할 수 있다.

- [Python documentation on iterators](https://docs.python.org/3/c-api/iterator.html)
- [Python documentation on generators](https://docs.python.org/3/c-api/gen.html)
- [Python tutorial on iterators and generators]()
- [Functional Programming HOWTO](https://docs.python.org/3/howto/functional.html)
